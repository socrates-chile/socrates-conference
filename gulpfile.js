var gulp = require("gulp"),
  connect = require("gulp-connect"),
  less = require("gulp-less"),
  uglify = require("gulp-uglify"),
  cleanCss = require("gulp-clean-css"),
  sourcemaps = require("gulp-sourcemaps"),
  concat = require("gulp-concat"),
  wait = require("gulp-wait"),
  htmlmin = require("gulp-htmlmin");

var cleanCssOptions = {
  level: {
    1: {
      all: true,
      specialComments: false,
      roundingPrecision: false
    },
    2: {
      all: true
    }
  }
};

gulp.task("default", ["build"]);
gulp.task("build", [
  "_build-less",
  "_build-html",
  "_build-fonts",
  "_build-imgs",
  "_build-videos",
  "_build-scripts"
]);

gulp.task("watch",/* ["build"], */function () {
  connect.server({
    root: "src",
    livereload: true
  });
  // gulp.watch(["src/*.html"], ["_build-html"]);
  // gulp.watch(["src/img/**"], ["_build-imgs"]);
  // gulp.watch(["src/js/**/*.js"], ["_build-scripts"]);
  // gulp.watch(["src/video/**"], ["_build-videos"]);
  // gulp.watch(["src/css/*.{less,css}"], ["_build-less"]);
});

gulp.task("_build-html", [], function () {
  return gulp
    .src(["src/*.html"])
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("dist"))
    .pipe(wait(500))
    .pipe(connect.reload());
});

gulp.task("_build-imgs", [], function () {
  return gulp
    .src(["src/img/**/*"])
    .pipe(gulp.dest("dist/img"))
    .pipe(connect.reload());
});
gulp.task("_build-fonts", [], function () {
  return gulp
    .src(["node_modules/font-awesome/fonts/*.*"])
    .pipe(gulp.dest("dist/fonts"))
    .pipe(connect.reload());
});
gulp.task("_build-videos", [], function () {
  return gulp
    .src(["src/video/**/*"])
    .pipe(gulp.dest("dist/video"))
    .pipe(connect.reload());
});
gulp.task("_build-scripts", [], function () {
  return gulp
    .src(["src/js/**/*.js"])
    .pipe(gulp.dest("dist/js"))
    .pipe(wait(500))
    .pipe(connect.reload());
});

gulp.task("_build-less", [], function () {
  return (
    gulp
      .src(["src/css/*.less"])
      // .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(cleanCss(cleanCssOptions))
      // .pipe(sourcemaps.write())
      .pipe(gulp.dest("dist/css"))
      .pipe(wait(500))
      .pipe(connect.reload())
  );
});
